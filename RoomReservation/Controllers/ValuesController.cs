﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RoomReservation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomReservationsController : ControllerBase
    {
        // POST api/values
        [HttpPost]
        public bool Post([FromBody] Reservation reservation)
        {
            if (!RoomsAvailable(reservation))
            {
                return false;
            }

            CreateGuestAsync(reservation.Guest);

            if (CreateReservationAsync(reservation))
            {
                //event an queue rausschicken
                return true;
            }
            else
            {
                return false;
            }
        }

        //validierungen, ob hotel, zimmer vorhanden
        private bool RoomsAvailable(Reservation reservation)
        {

            //GET an Gruppe 1
            return false;
        }

        private async Task CreateGuestAsync(Guest guest)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("/");
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/USERS", guest);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //POST an api\USERS
        }

        private async Task<bool> CreateReservationAsync(Reservation reservation)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("/");
            try
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("api/USERS", reservation);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return true;
            //anlegen in unserer Datenhaltung 
        }
    }
}
