﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace RoomReservation
{
    public class Reservation
    {
        public Guest Guest { get; set; }
        public Hotel Hotel { get; set; }
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
        public List<Room> Rooms { get; set; }
        public bool Paid { get; set; }

    }
    public class Room
    {
        public string Category { get; set; }
        public int count { get; set; }
    }
    public class Hotel
    {
        public string Name { get; set; }
        public List<Room> Rooms { get; set; }
    }

    public class Guest
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Title { get; set; }
     }
}
